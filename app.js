const mongoose = require('mongoose');
require('dotenv').config();

mongoose.Promise = global.Promise;
mongoose.connect(process.env.DB_URL);
mongoose.connection.on('error', (err) => {
    if (err) {
        throw new Error(`Connection failed ${err.toString()}`);
    }
});

const app = require('./config/express');
const adminRoutes = require('./src/modules/admin/routes');

app.use(adminRoutes);

app.listen(process.env.PORT, (err) => {
    if (err) {
        throw new Error(`Failed connecting to port: ${process.env.PORT}`);
    }
    throw new Error(`Server's running on port: ${process.env.PORT}`);
});
