module.exports = {
    "env": {
        "node": true,
        "es6": true
    },
    "extends": "airbnb-base",
    "rules": {
        "indent": 0,
        "consistent-return": 0,
        "no-underscore-dangle": 0,
        "no-tabs": 0,
        "dot-notation":0,
        "linebreak-style": ["error", "windows"],
        "comma-dangle": ["error", "never"],
        "import/no-unresolved": "off"
    }
};