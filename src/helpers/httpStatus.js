module.exports = {
    OK: {
        status: 200,
        message: 'OK'
    },
    MOVED_PERMANENTLY: {
        status: 301,
        message: 'Moved Permanently'
    },
    FOUND: {
        status: 302,
        message: 'Found'
    },
    TEMPORARY_REDIRECT: {
        status: 307,
        message: 'Temporary Redirect '
    },
    BAD_REQUEST: {
        status: 400,
        message: 'Bad Request'
    },
    UNAUTHORIZED: {
        status: 401,
        message: 'Unauthorized'
    },
    FORBIDDEN: {
        status: 403,
        message: 'Forbidden'
    },
    NOT_FOUND: {
        status: 404,
        message: 'Not Found'
    },
    INTERNAL_SERVER_ERROR: {
        status: 500,
        message: 'Internal Server Error'
    }
};
