const mongoose = require('mongoose');
const getSlug = require('speakingurl');

const { Schema } = mongoose;

const blogCategorySchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    url: {
        type: String,
        required: true,
        trim: true
    }
}, {
    timestamps: true
});

blogCategorySchema.pre('save', (next) => {
    const category = this;

    if (!category.isModified('name')) return next();

    const url = getSlug(category.name, {
        lang: 'vn'
    });

    category.url = url;

    next();
});

module.exports = mongoose.model('blog_categories', blogCategorySchema);
