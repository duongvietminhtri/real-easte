const mongoose = require('mongoose');
const getSlug = require('speakingurl');

const { Schema } = mongoose;

const blogPostSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    title: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String,
        required: true,
        trim: true
    },
    content: {
        type: String,
        required: true,
        trim: true
    },
    images: {
        cover: {
            type: Schema.Types.ObjectId,
            ref: 'upload',
            required: true
        },
        post: [{
            type: Schema.Types.ObjectId,
            ref: 'upload'
        }]
    },
    url: {
        type: String,
        trim: true
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    tags: [{
        type: String,
        trim: true
    }],
    categories: [
        {
            type: Schema.Types.ObjectId,
            ref: 'categories'
        }
    ]
}, {
    timestamps: true
});

blogPostSchema.pre('save', function optimizeSlug(next) {
    const blog = this;

    if (!blog.isModified('title')) return next();

    const url = getSlug(blog.title, {
        lang: 'vn'
    });

    blog.url = url;

    next();
});

module.exports = mongoose.model('blog_posts', blogPostSchema);
