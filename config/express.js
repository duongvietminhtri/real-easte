const express = require('express');
const morgan = require('morgan');

const app = express();

app.use(morgan('dev'));

app.use('/public', express.static('./src/public'));

module.exports = app;
